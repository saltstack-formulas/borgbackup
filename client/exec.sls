{% for borgserver, borgdata in salt['pillar.get']('borg:servers').items() %}
 {% set PASSPHRASE = borgdata['passphrase'] %}
 {% set current_path = salt['environ.get']('PATH', '/bin:/usr/bin') %}

 {% if borgdata['pool'] in grains['pool'] %}

initialize host repo {{ loop.index }}:
  cmd.run:
    - name: "borgmatic init --append-only --encryption repokey"
    - shell: /bin/bash
    - ignore_timeout: true
    - env:
        - PATH: {{ [current_path, '/root/.local/bin']|join(':') }}
        - BORG_PASSPHRASE: {{ PASSPHRASE }}

first archive {{ loop.index }}:
  cmd.run:
    - name: at 22:00 -f /usr/local/sbin/backupinit.sh
    - shell: /bin/bash

 {% endif %}
{% endfor %}