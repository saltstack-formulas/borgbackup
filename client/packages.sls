install apt packages:
  pkg.installed:
    - pkgs:
      - python3
      - python3-dev
      - libssl-dev
      - openssl
      - build-essential
      - libfuse-dev
      - fuse
      - pkg-config
      - libacl1-dev
      - libacl1
      - libzstd1
      - python3-pip
      - at

install pip cython:
  pip.installed:
    - name: cython
    - require:
      - pkg: install apt packages
install pip borg:
  pip.installed:
    - name: borgbackup
    - bin_env: /usr/bin/pip3
    - require:
      - pip: install pip cython
install pip borgmatic:
  pip.installed:
    - name: borgmatic
    - bin_env: /usr/bin/pip3
    - require:
      - pip: install pip borg