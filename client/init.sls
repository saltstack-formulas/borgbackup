{% set fulldict = {} %}
#{#% set hosts211 = salt['pillar.get']('machines211', {}) %#}
{% set hosts212 = salt['pillar.get']('machines212', {}) %}
#{#% do fulldict.update(hosts211) %#}
{% do fulldict.update(hosts212) %}

{% for host, hostinfo in fulldict.items() %}
 {% if 'RsyncShareName' in hostinfo %}
  {% if 'SaltHostname' in hostinfo %}
   {% set host = grains['id'] %}
   {% if hostinfo['SaltHostname'] == host %}

include:
  - .client.packages
  - .client.files
  - .client.exec
  - .client.crons

   {% endif %}
  {% endif %}
 {% endif %}
{% endfor %}
  