1. Be sure that the key has been added on the borg server (see `borgbackup.add_clients`) and the remote directory exists.
  If not, refresh mines `salt '*' mine.update`, then re-run `borgbackup.add_clients` on borg server (check pillar `pillar.example`),
2. If the machine does not have enough memory, borgbackup build may be killed by the OS (2GB is enough),
3. By default, this recipe set the repository in append-only mode, avoiding issue with data corruptions,
4. SSH key used to access to the repository is the root default ssh key on client (without passphrase), and the server host ssh fingerprint is `ssh-rsa`.

Usage:

```bash
salt '$minion' state.sls borgbackup.client
```
