{% set fulldict = {} %}
#{#% set hosts211 = salt['pillar.get']('machines211', {}) %#}
{% set hosts212 = salt['pillar.get']('machines212', {}) %}
#{#% do fulldict.update(hosts211) %#}
{% do fulldict.update(hosts212) %}

{% for host, hostinfo in fulldict.items() %}
 {% if 'RsyncShareName' in hostinfo %}
  {% if 'SaltHostname' in hostinfo %}
   {% set host = grains['id'] %}
   {% if hostinfo['SaltHostname'] == host %}

/etc/borgmatic:
  file.directory:
    - user: root
    - group: root
    - dir_mode: '0700'
/etc/borgmatic/config.yaml:
  file.managed:
    - source: salt://borgbackup/client/borgmatic.config.yaml
    - template: jinja
    - context:
        HOSTINFO: {{ hostinfo }}
/usr/local/sbin/backupinit.sh:
  file.managed:
    - source: salt://borgbackup/client/backupinit.sh.jinja
    - user: root
    - group: root
    - mode: '0700'
    - template: jinja
    {% for borgserver, borgdata in salt['pillar.get']('borg:servers').items() %}
     {% if borgdata['pool'] in grains['pool'] %}
adding Host fingerprint in known_hosts {{ loop.index }}:
  ssh_known_hosts.present:
    - name: {{ borgserver }}
    - user: root
    - key: {{ borgdata['host_key'] }}
    - enc: {{ borgdata['host_key_enc'] }}
     {% endif %}
    {% endfor %}
  
   {% endif %}
  {% endif %}
 {% endif %}
{% endfor %}