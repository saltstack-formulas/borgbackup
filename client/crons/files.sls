
{%- set fulldict = {} %}
#{#%- set hosts211 = salt['pillar.get']('machines211', {}) %#}
{%- set hosts212 = salt['pillar.get']('machines212', {}) %}
#{#%- do fulldict.update(hosts211) %#}
{%- do fulldict.update(hosts212) %}

{%- for host, hostinfo in fulldict.items() %}
 {% if 'RsyncShareName' in hostinfo %}
  {% if 'SaltHostname' in hostinfo %}
   {% set host = grains['id'] %}
   {% if hostinfo['SaltHostname'] == host %}

/etc/cron.d/borgmatic:
  file.managed:
    - user: root
    - group: root
    - mode: '0600'
    - source: salt://borgbackup/client/crons/borgmatic/borgmatic.cron
    - template: jinja
    - onlyif:
        - command -v borg

/var/log/borg:
  file.directory

/etc/cron.d/jsoncheck:
  file.managed:
    - user: root
    - group: root
    - mode: '0600'
    - source: salt://borgbackup/client/crons/borgmatic/json_check.cron
    - template: jinja
    - onlyif:
        - command -v borg

   {% endif %}
  {% endif %}
 {% endif %}
{% endfor %}