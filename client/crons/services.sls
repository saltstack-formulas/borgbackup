{% if grains["os_family"] == "RedHat" %}
crond:
  service.running:
    - enable: True
    - reload: True
{% else %}
cron:
  service.running:
    - enable: True
    - reload: True
{% endif %}

