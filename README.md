# BorgBackup SaltStack formula

> Note: this formula is provided "as is" without any warranty of any kind. It has been tested successfully on my debian-like minions.

## Requirements

First, you need a working [`borgbackup`](https://www.borgbackup.org/) server (in fact, it just needs ssh and a huge amount of free space).

Then, you need to setup a SaltStack grain's list named `pool` to determine current network subnets. Take a look at [this formula](https://gitlab.mbb.univ-montp2.fr/saltstack-formulas/set_grains) if needed.
For example, here I am using `isem212` for our network `192.168.212.0/24`.

Then, check the `pillar.example` file to manage your borg server(s).

## Usage

### Server

The `borbackup.server` formula will then add the borg user on the server.

```bash
# $minion_borg_server is your borg server's SaltStack minion id
salt '$minion_borg_server' -v state.sls borgbackup.server
```

### SSH keys

In order to add the client rsa key, you may need to create the key.
In that case, you will just have to apply that formula:

```bash
salt '$minion' -v state.sls borgbackup.generate_rsa_key
```

Then, refresh mines and apply the add_clients recipe, to sync ssh keys:

```bash
salt '*' mine.update
salt '$minion_borg_server' -v state.sls borgbackup.add_clients
```

### Client configuration and packages

To configure backup directories, please take a look at `hosts212.pillar.example`. For a more general client configuration, change the template `client/borgmatic.config.yaml`. You may need to overwrite it with a more complex pillar...

By default the retention policy is:

```
retention:
    # Number of daily archives to keep.
    keep_daily: 7
    # Number of weekly archives to keep.
    keep_weekly: 3
    # Number of monthly archives to keep.
    keep_monthly: 2
```

`borgbackup.client` will then look at `RsyncShareName` values and `BackupFilesExclude` (I kept the BackupPC naming format for formula compatibility).

The borgclient recipe installs `borgbackup` and [`borgmatic`](https://torsion.org/borgmatic/) using `pip3`.

`borgmatic` allows us to use an easier way to manage the borg backups.

```bash
salt '$minion' -v state.sls borgbackup.client
```

References:

  - https://torsion.org/borgmatic/docs/how-to/set-up-backups/
  - https://doc.ubuntu-fr.org/borgbackup
  - https://sebsauvage.net/wiki/doku.php?id=borgbackup
  - https://www.youtube.com/watch?v=QIMJdnghEmU
  - https://borgbackup.readthedocs.io/en/stable/deployment/central-backup-server.html
  - https://wiki.fiat-tux.fr/books/administration-syst%C3%A8mes/page/borgmatic
  - https://torsion.org/borgmatic/docs/how-to/set-up-backups/

### Comparing to BackupPC

With BorgBackup, there is no real Borg server, contrary to BackupPC. Indeed, by default, there is no AdminUI/WebUI.
Moreover, connections are done by SSH from the clients; they connect to the server to backup their data.
With BackupPC, it is the opposite : connections are initialized by the server to retrieve data on the clients.

However, with deduplication and encryption, BorgBackup is really more powerful.
