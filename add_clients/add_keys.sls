{% set allkeys = salt['mine.get']('*', 'get_ssh_rsa_key') %}
{% set BORGDATA = salt['pillar.get']('borg:servers') %}
{% for server, data in BORGDATA.items() %}
 {% if server == grains['fqdn_ip4'][0] %}
  {% set mainrepo = data['repo'] %}
  {% for host, rsakey in allkeys.items() %}

creating empty directory repo for {{ host }}:
  cmd.run:
    - name: "mkdir -p {{ mainrepo }}/{{ host }}"
    - shell: /bin/bash
    - runas: {{ data['borguser'] }}

# CAUTION!
# If you change the ssh command= option below, it won't necessarily get pushed to the backup
# server correctly unless you delete the ~/.ssh/authorized_keys file and re-create it!
adding_rsa_key_{{ host }}:
  ssh_auth.present:
    - user: {{ data['borguser'] }}
    - enc: {{ data['host_key_enc'] }}
    - names:
      - {{ rsakey }}
    - options:
      - command="cd {{ mainrepo }}/{{ host }}; borg --umask=077 serve --append-only --restrict-to-path {{ mainrepo }}/{{ host }}"
      - restrict

  {% endfor %}
 {% endif %}
{% endfor %}