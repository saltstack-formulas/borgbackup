This recipe is helpful to add ssh authorized_keys on the borg server. SSH RSA Keys are taken on each minion using an updated mine.
If the key does not exist on the server side, it is added by this formula.

This formula also create an empty directory for the client host, to allow it to create a borg backup repository on the borg server.

Usage:

```bash
salt '$minion_borg_server' state.sls borgbackup.add_clients
```
