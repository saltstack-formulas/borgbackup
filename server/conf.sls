# from https://borgbackup.readthedocs.io/en/stable/deployment/central-backup-server.html#salt

Install borg backup from pip:
  pkg.installed:
    - pkgs:
      - python3
      - python3-dev
      - python3-pip
      {% if grains['osmajorrelease'] < 20 %}
      - python-virtualenv
      {% else %}
      - python3-virtualenv
      {% endif %}
      - libssl-dev
      - openssl
      - libacl1-dev
      - libacl1
      - build-essential
      - libfuse-dev
      - fuse
      - pkg-config
  pip.installed:
    - pkgs: ["borgbackup"]
    - bin_env: /usr/bin/pip3

{% set BORGDATA = salt['pillar.get']('borg:servers') %}
{% for server, data in BORGDATA.items() %}
 {% if server == grains['fqdn_ip4'][0] %}

Check root for home dir:
  file.directory:
    - name: {{ data['home'] }}
    - makedirs: True

Setup backup user:
  user.present:
    - name: {{ data['borguser'] }}
    - fullname: Backup User
    - home: {{ data['home'] }}
    - uid: 4040
    - gid: 4040
    - shell: /bin/bash

Check root for borg dir:
  file.directory:
    - name: {{ data['repo'] }}
    - makedirs: True
    - user: borg
    - group: borg
    - recurse:
      - user
      - group
      - mode
 {% endif %}
{% endfor %}

# adding ssh keys has been moved to borgbackup.add_clients