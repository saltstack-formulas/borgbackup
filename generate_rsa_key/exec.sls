generate missing key:
  cmd.run:
    - name: ssh-keygen -a 100 -t rsa -f /root/.ssh/id_rsa -N '' -q
    - shell: /bin/bash
    - unless: ls /root/.ssh/id_rsa.pub
