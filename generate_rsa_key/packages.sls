
needed packages for ssh client:
  pkg.installed:
    - pkgs:
{% if grains['os_family']  == 'Debian' %}
      - openssh-client
      - libssl1.1
{% else %}
      - openssh
      - openssl-libs
{% endif %}